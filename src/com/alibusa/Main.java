package com.alibusa;

public class Main {

    public static void main(String[] args) {

        //Empty Constructors
            User a = new User();
                a.setFirstName("Brendon");
                a.setLastName("Urie");
                a.setContactNumber("09123123123");
                    System.out.println("First Name: " + a.getFirstName());
                    System.out.println("Last Name: " + a.getLastName());
                    System.out.println("Contact Number: " + a.getContactNumber());

        // Parameterized Constructors
            User b = new User("Spencer", "Smith", "09456456456");
                System.out.println("** Parameterized Constructors **");
                System.out.println("First Name: " + b.getFirstName());
                System.out.println("Last Name: " + b.getLastName());
                System.out.println("Contact Number: " + b.getContactNumber());

        // Method : Display Details
            User c = new User("Ryan", "Ross", "09789789789");
                System.out.println("** Method : Display Details **");
                System.out.println(c.details());
    }
}
