package com.alibusa;

public class User {

    private String firstName;
    private String lastName;
    private String contactNumber;

    //Empty Constructors
        public User() {
            System.out.println("** Empty Constructors **");
        }

        // Getters
        public String getFirstName() {
            return this.firstName;
        }
        public String getLastName() {
            return this.lastName;
        }
        public String getContactNumber() {
            return this.contactNumber;
        }

        // Setters
        public void setFirstName(String newFirstName) {
            this.firstName = newFirstName;
        }
        public void setLastName(String newLastName) {
            this.lastName = newLastName;
        }
        public void setContactNumber(String newContactNumber) {
            this.contactNumber = newContactNumber;
        }

    // Parameterized Constructors
        public User(String newFirstName, String newLastName, String newContactNumber) {
            this.firstName = newFirstName;
            this.lastName = newLastName;
            this.contactNumber = newContactNumber;
        }

    // Method : Display Details
        public String details() {
            return "Hello, " + this.getFirstName() + " " + this.getLastName() + "! Your Contact Number is: " + this.getContactNumber() + ".";
        }
}
